# this file will be used to model all technologies

from data_transfer import DataManager

from pulp import *
import pandas as pd

# convention:
# i.e. v_h_hp
# v -> output, u -> input
# h -> heat, e -> electricity, g -> gas
# hp -> technology


class Heatpump:
    def __init__(self, creator, balance, time):
        # handle all variables and constraint from heatpump

        # readine timeinstant
        self.time = time

        # get lp object
        self.creator = creator
        self.lp_obj = creator.my_LP

        # define class internal variables
        # get inputs
        self.cop_hp = creator.data_manager.get_value(creator.static_data, 'cop_hp', 'value')
        self.hp_cap_inst = creator.data_manager.get_value(creator.static_data, 'cap_inst_hp', 'value')

        self.E_fp = creator.E_fp

        # define variables
        # heat ouput
        self.v_h_hp = pulp.LpVariable('v_h_hp_' + str(self.time))
        self.lp_obj += self.v_h_hp >= 0
        # electricity input
        self.u_e_hp = pulp.LpVariable('u_e_hp_' + str(self.time))
        self.lp_obj += self.u_e_hp >= 0

        # add variables to balance
        # heat
        balance.heat_balance += self.v_h_hp
        # electricity
        balance.electricity_balance += - self.u_e_hp
        # emissions
        creator.emission_balance += self.E_fp * self.u_e_hp

        self.__build_constraints()

        pass

    def __build_constraints(self):
        # build constraints for heatpump
        # non-negative condition on heat output | ceiling installed capacity: heat output + existing
        self.lp_obj += 0 <= self.v_h_hp <= self.creator.cap_inv_hp + self.hp_cap_inst
        # conversion efficiency
        self.lp_obj += self.u_e_hp * self.cop_hp == self.v_h_hp
        return


class Boiler:
    def __init__(self, creator, balance, time):
        # handle all variables and constraint from boiler

        # redefine time
        self.time = time

        # get lp_ob
        self.creator = creator
        self.lp_obj = creator.my_LP

        # define class internal variables
        # get inputs
        self.eta_b = creator.data_manager.get_value(creator.static_data, 'eta_b', 'value')
        self.cap_inst_boiler = creator.data_manager.get_value(creator.static_data, 'cap_inst_b', 'value')
        self.G_fp = creator.G_fp

        # define variables
        self.v_h_b = pulp.LpVariable('v_h_b_' + str(self.time))
        self.lp_obj += self.v_h_b >= 0
        self.u_g_b = pulp.LpVariable('u_g_b_' + str(self.time))
        self.lp_obj += self.u_g_b >= 0

        self.__build_constraints(creator, balance)
        pass

    def __build_constraints(self, creator, balance):
        # build constraints for boiler
        # non-negative condition on heat output | ceiling installed capacity: heat output + existing
        self.lp_obj += self.v_h_b <= creator.cap_inv_b + self.cap_inst_boiler

        # add variables to balance
        # heat
        balance.heat_balance += self.v_h_b
        # gas
        balance.gas_balance += - self.u_g_b

        # emissions
        creator.emission_balance += self.u_g_b * self.G_fp

        # conversion efficiency
        self.lp_obj += self.u_g_b * self.eta_b >= self.v_h_b
        return


class Solarpower:
    def __init__(self, creator, balance, time):
        # handle all variables and constraint from electric solar power
        # @Sven

        # redefine time
        self.time = time

        # get lp_ob
        # self.creator = creator
        self.lp_obj = creator.my_LP

        # define class internal variables
        # get inputs
        self.cap_inst_pv = creator.data_manager.get_value(creator.static_data, 'cap_inst_pv') # in kW
        self.area_pv = creator.data_manager.get_value(creator.static_data, 'area_pv')          # kW/m^2
        self.solar_irradiation = creator.data_manager.get_value(creator.data_manager.dynamic_data, self.time, 'solar_irradiation')

        # define variables
        self.v_e_pv = pulp.LpVariable('v_e_pv_' + str(self.time))

        # add variables to balance
        # electricity
        balance.electricity_balance += self.v_e_pv

        # no operation emissions

        self.__build_constraints(creator)

        pass

    def __build_constraints(self, creator):
        # build constraints for Solarpower
        # non-negative condition on electricity output
        self.lp_obj += 0 <= self.v_e_pv
        self.lp_obj += self.v_e_pv == (creator.cap_inv_pv + self.cap_inst_pv) * self.solar_irradiation / self.area_pv 

        return


class Batteries:
    def __init__(self, creator, balance_tracker):
        # handle all variables and constraint from batteries

        # get lp_ob
        self.lp_obj = creator.my_LP
        self.data_manager = creator.data_manager

        # define parameters
        # add import from static data
        self.cap_ins_bat = self.data_manager.get_value(self.data_manager.static_data, 'cap_inst_bat')
        self.inv_cost_bat = self.data_manager.get_value(self.data_manager.static_data, 'lic_bat')

        # properties of Li batteries
        # max charging/discharging rate
        self.max_charge_rate = 0.1
        self.s_discharge = 0.001
        # charging/discharging efficiency
        self.eta_bat = 0.95

        self.variable_dic = {}

        # defining variable
        self.cap_inv_bat = pulp.LpVariable("cap_inv_bat")
        self.lp_obj += self.cap_inv_bat >= 0

        # add investition costs to cost balance
        creator.cost_balance += self.cap_inv_bat * self.inv_cost_bat

        # add emissions from production to emission balance
        em_bat = self.data_manager.get_value(self.data_manager.static_data, 'prod_em_bat')

        creator.emission_balance += self.cap_inv_bat * em_bat

        # store value for export
        creator.inv_dic[self.cap_inv_bat.name.split('_')[-1]] = [self.inv_cost_bat, em_bat]

        iteration_hours = self.data_manager.dynamic_data.index.values.tolist()
        for i in iteration_hours:
            self.__add_constraints(i, creator, balance_tracker)

        # same soc at beginning and end of tim period (zero)
        self.lp_obj += self.variable_dic[min(iteration_hours)][2] == self.variable_dic[max(iteration_hours)][2]
        # self.lp_obj += self.variable_dic[max(iteration_hours)][2] == 0

        pass

    def __add_constraints(self, time, creator, balance_tracker):
        # build constraints for battery
        # main constraint
        # variables stored in list, ordering battery 0:output, battery 1:input, 2:current soc, 3:binary variable
        self.variable_dic[time] = [pulp.LpVariable('v_e_bat_' + str(time)), pulp.LpVariable('u_e_bat_' + str(time)), pulp.LpVariable('soc_e_bat_' + str(time)), pulp.LpVariable('bat_charging_' + str(time), cat='Binary')]

        # Non-negativity constraints
        self.lp_obj += self.variable_dic[time][0] >= 0
        self.lp_obj += self.variable_dic[time][1] >= 0
        self.lp_obj += self.variable_dic[time][2] >= 0

        # decision: charge or discharge
        # big-M:
        M = 10000000
        # self.lp_obj += self.variable_dic[time][0] <= M * self.variable_dic[time][3]
        # self.lp_obj += self.variable_dic[time][1] <= M * (1-self.variable_dic[time][3])

        # calculating state of charge
        # soc[t] = soc[t-1] + u_e[t-1] - v_e[t-1]
        if time-1 in self.variable_dic.keys():
            self.lp_obj += self.variable_dic[time][2] == (1-self.s_discharge)*self.variable_dic[time-1][2] - self.variable_dic[time-1][0] * 1 / self.eta_bat + self.eta_bat *self.variable_dic[time-1][1]

        # Adding to balance
        balance_tracker[time].electricity_balance += self.variable_dic[time][0]
        balance_tracker[time].electricity_balance += -self.variable_dic[time][1]


        # defining upper bound constraints
        self.lp_obj += 0 <= self.variable_dic[time][1]
        self.lp_obj += self.variable_dic[time][1] <= self.max_charge_rate * (self.cap_inv_bat + self.cap_ins_bat)
        self.lp_obj += self.variable_dic[time][0] <= self.max_charge_rate * (self.cap_inv_bat + self.cap_ins_bat)
        self.lp_obj += 0 <= self.variable_dic[time][2] <= self.cap_inv_bat + self.cap_ins_bat

        return


class Hydropower:
    def __init__(self, creator, balance_tracker):
        # @Hakon
        # recursive function; might need different inputs

        # get lp_ob
        self.lp_obj = creator.my_LP
        self.data_manager = creator.data_manager

        # defining parameters
        self.max_cap_hydro = creator.data_manager.get_value(creator.static_data, 'cap_inst_hydro')
        self.current_cap_hydro = creator.data_manager.get_value(creator.static_data, 'cap_inst_hydro')
        self.max_resevoir_hydro = creator.data_manager.get_value(creator.static_data, 'max_resevoir_hydro')
        self.eta_hydro = creator.data_manager.get_value(creator.static_data, 'eta_hydro')
        self.cost_hydro = creator.data_manager.get_value(creator.static_data, 'hydro_price')

        # get inflow data
        self.hydro_inflow = self.data_manager.hydro_inflow

        # defining time independent variable
        # if investing in hydropower is enabled, add investment costs
        self.cap_inv_hydro = pulp.LpVariable("cap_inv_hydro")
        self.lp_obj += self.cap_inv_hydro == 0

        self.res_inv_hydro = pulp.LpVariable("res_inv_hydro")
        self.lp_obj += self.res_inv_hydro == 0


        # dictionary to store variables
        self.variable_dic = {}

        iteration_hours = self.data_manager.dynamic_data.index.values.tolist()
        for i in iteration_hours:
            self.__add_constraints(i, creator, balance_tracker)

        self.lp_obj += self.variable_dic[min(iteration_hours)][1] == self.variable_dic[max(iteration_hours)][1]

        pass

    def __add_constraints(self, time, creator, balance_tracker):
        # build constraints for hydropower

        # define variables
        self.variable_dic[time] = [pulp.LpVariable('v_e_hydro_' + str(time)), pulp.LpVariable('hydro_res_' + str(time))]
        self.lp_obj += self.variable_dic[time][1] >= 0
        self.lp_obj += self.variable_dic[time][0] >= 0

        # main constraint
        if time-1 in self.variable_dic.keys():
            self.lp_obj += self.variable_dic[time][1] == self.variable_dic[time-1][1] + self.hydro_inflow[time-1] - \
                           self.variable_dic[time-1][0] * 1 / self.eta_hydro

        # defining upper bound constraints
        self.lp_obj += self.variable_dic[time][1] <= self.max_resevoir_hydro + self.res_inv_hydro
        self.lp_obj += self.variable_dic[time][0] <= self.max_cap_hydro + self.cap_inv_hydro

        # Adding to balance
        balance_tracker[time].electricity_balance += self.variable_dic[time][0]

        # Adding costs
        creator.cost_balance += self.variable_dic[time][0] * self.cost_hydro

        return


class Pumpedhydro:
    def __init__(self, creator, balance_tracker):
        # handle all variables and constraint from batteries
        # @Hakon
        # recursive function; might need different inputs
        # redefine time
        self.variable_dic = {}
        self.lp_obj = creator.my_LP
        self.data_manager = creator.data_manager
        # get lp_ob

        # defining parameters
        self.max_cap_ph = creator.data_manager.get_value(creator.static_data, 'cap_inst_ph', 'value')
        self.max_resevoir_ph = creator.data_manager.get_value(creator.static_data, 'max_resevoir_ph', 'value')
        self.eta_ph = creator.data_manager.get_value(creator.static_data, 'eta_ph', 'value') # Note: Can change this to be different for pump/turbine
        self.cost_ph = creator.data_manager.get_value(creator.static_data, 'hydro_price', 'value')

        # defining variables
        self.cap_inv_ph = pulp.LpVariable("cap_inv_ph")
        self.lp_obj += self.cap_inv_ph >= 0
        self.ph_inflow = self.data_manager.ph_inflow

        iteration_hours = self.data_manager.dynamic_data.index.values.tolist()
        for i in iteration_hours:
            self.__add_constraints(i, creator, balance_tracker)
        self.lp_obj += self.variable_dic[min(iteration_hours)][2] == self.variable_dic[max(iteration_hours)][2]

        pass

    def __add_constraints(self, time, creator, balance_tracker):
        # Defining variable dictionary
        self.variable_dic[time] = [pulp.LpVariable('v_e_ph_' + str(time)), pulp.LpVariable('u_e_ph_' + str(time)), pulp.LpVariable('ph_res_' + str(time))]

        # Resevoir constraint
        if time-1 in self.variable_dic.keys():
            self.lp_obj += self.variable_dic[time][2] == self.variable_dic[time-1][2] + self.ph_inflow[time-1] - self.variable_dic[time-1][0] * 1 / self.eta_ph + self.variable_dic[time-1][1] * self.eta_ph

        # Upper bound constraints
        self.lp_obj += self.variable_dic[time][2] <= self.max_resevoir_ph
        self.lp_obj += self.variable_dic[time][0] <= self.max_cap_ph
        self.lp_obj += self.variable_dic[time][1] <= self.max_cap_ph

        # Non-negativity constraints
        self.lp_obj += 0 <= self.variable_dic[time][0]
        self.lp_obj += 0 <= self.variable_dic[time][1]
        self.lp_obj += 0 <= self.variable_dic[time][2]

        # Adding to balance
        balance_tracker[time].electricity_balance += self.variable_dic[time][0]
        balance_tracker[time].electricity_balance += -self.variable_dic[time][1]

        # Adding costs
        creator.cost_balance += self.variable_dic[time][0] * self.cost_ph

        return


class GasTurbines:
    def __init__(self, creator, balance, time):
        # handle all variables and constraint from gas turbines

        # redefine variables
        self.time = time
        self.lp_obj = creator.my_LP
        self.creator = creator
        self.G_fp = creator.G_fp

        # Gas turbine parameters
        self.eta_gt = creator.data_manager.get_value(creator.data_manager.static_data, 'eta_gt')
        self.cap_inst_gt = creator.data_manager.get_value(creator.data_manager.static_data, 'cap_inst_gt')

        # Gas turbine variables
        self.v_e_gt = pulp.LpVariable('v_e_gt_' + str(self.time))
        self.lp_obj += self.v_e_gt >= 0
        self.u_g_gt = pulp.LpVariable('u_g_gt_' + str(self.time))
        self.lp_obj += self.u_g_gt >= 0

        self.__build_constraints(creator, balance)

        pass

    def __build_constraints(self, creator, balance):
        # build constraints for Gas turbine

        # define conversion efficiency
        self.lp_obj += self.v_e_gt <= self.u_g_gt * self.eta_gt

        # add variables to balances
        # emissions
        creator.emission_balance += self.u_g_gt * self.G_fp
        # electricity
        balance.electricity_balance += self.v_e_gt
        # gas balance
        balance.gas_balance += - self.u_g_gt

        # defining upper bound constraints for capacity
        self.lp_obj += self.v_e_gt <= creator.cap_inv_gt + self.cap_inst_gt

        return


class NuclearPower:
    def __init__(self, creator, balance_tracker):
        # handle all variables and constraint from boiler

        # redefine instances
        self.creator = creator
        self.lp_obj = creator.my_LP
        self.data_manager = creator.data_manager

        # get data
        self.max_capacity = self.data_manager.get_value(self.data_manager.static_data, 'cap_inst_nuc')
        self.max_gradient = self.data_manager.get_value(self.data_manager.static_data, 'gradient_h_nuc') * self.max_capacity
        self.min_capacity = self.data_manager.get_value(self.data_manager.static_data, 'min_power_nuc') * self.max_capacity
        self.price_nuclear = self.data_manager.get_value(self.data_manager.static_data, 'nuc_price')

        # dictionary to store variables
        self.variable_dic = {}

        # additionally installed capacity
        # zero as forbidden
        cap_inv_nuc = pulp.LpVariable('cap_inv_nuc')
        self.lp_obj += cap_inv_nuc == 0

        # iterate over all instances
        # list of hours
        iteration_hours = self.data_manager.dynamic_data.index.values.tolist()

        for i in iteration_hours:
            self.__add_constraints(i, creator, balance_tracker)

        return

    def __add_constraints(self, time, creator, balance_tracker):
        # add variables and constraints for every hour

        # electricity output
        self.variable_dic[time] = pulp.LpVariable('v_e_nuc_' + str(time))

        # min / max constraint
        self.lp_obj += self.variable_dic[time] >= 0
        self.lp_obj += self.min_capacity <= self.variable_dic[time]
        self.lp_obj += self.variable_dic[time] <= self.max_capacity

        # limit difference to previous hour

        if time-1 in self.variable_dic.keys():
            self.lp_obj += self.variable_dic[time-1] - self.max_gradient <= self.variable_dic[time]
            self.lp_obj += self.variable_dic[time] <= self.variable_dic[time-1] + self.max_gradient

        # add variables to balance
        # electricity
        balance_tracker[time].electricity_balance += self.variable_dic[time]
        creator.cost_balance += self.variable_dic[time] * self.price_nuclear
        return


class RunOfRiver:
    def __init__(self, creator, balance, time):
        # handle all variables and constraint from gas turbines

        # redefine variables
        self.time = time
        self.lp_obj = creator.my_LP
        self.creator = creator
        self.data_manager = creator.data_manager

        # import data
        self.eta_ror = self.data_manager.get_value(self.data_manager.static_data, 'eta_ror')
        self.price_ror = self.data_manager.get_value(self.data_manager.static_data, 'ror_price')
        # time-depending energy flow in rivers
        self.inflow_ror = self.data_manager.get_value(self.data_manager.dynamic_data, self.time, 'river_flow')
        # capacity factor for lower production limit
        self.cap_factor_ror = self.data_manager.get_value(self.data_manager.static_data, 'min_power_ror')

        # define variable
        self.v_e_ror = pulp.LpVariable('v_e_ror_' + str(self.time))
        self.lp_obj += self.v_e_ror >= 0

        # add variables to balance
        # electricity balance
        balance.electricity_balance += self.v_e_ror
        # cost balance
        creator.cost_balance += self.v_e_ror * self.price_ror

        self.__build_constraints()

        pass

    def __build_constraints(self):
        # build constraints for Run of River power plants
        # efficiency constraint
        self.lp_obj += self.v_e_ror <= self.inflow_ror * self.eta_ror

        # lower production constraint
        self.lp_obj += self.v_e_ror >= self.cap_factor_ror * self.inflow_ror * self.eta_ror
        return


class PowerToGas:
    def __init__(self, creator, balance_tracker):
        # handle all variables and constraint from batteries

        # get lp_ob
        self.lp_obj = creator.my_LP
        self.data_manager = creator.data_manager

        # define parameters
        # add import from static data
        # installed capacity power-2-gas
        self.cap_ins_p2g = self.data_manager.get_value(self.data_manager.static_data, 'cap_inst_ptog')
        # installed storage capacity for gas
        self.cap_ins_gas_storage = self.data_manager.get_value(self.data_manager.static_data, 'cap_inst_ptogstorage')

        # investment cost for power-2-gas
        self.inv_cost_p2g = self.data_manager.get_value(self.data_manager.static_data, 'lic_p2g')
        # investment cost for gas storage
        self.inv_cost_storage = self.data_manager.get_value(self.data_manager.static_data, 'lic_ptogstorage')
        # power-2-gas efficiency
        self.eta_p2g = self.data_manager.get_value(self.data_manager.static_data, 'eta_p2g')


        # defining variable
        self.cap_inv_p2g = pulp.LpVariable("cap_inv_ptog")
        self.lp_obj += self.cap_inv_p2g >= 0

        self.cap_inv_p2g_storage = pulp.LpVariable("cap_inv_ptogstorage")
        self.lp_obj += self.cap_inv_p2g_storage >= 0

        # emptying rate: fraction of max capacity
        self.r_out = 0.05

        self.variable_dic = {}

        # add investment costs to cost balance
        creator.cost_balance += self.cap_inv_p2g * self.inv_cost_p2g
        creator.cost_balance += self.cap_inv_p2g_storage * self.inv_cost_storage

        # add emissions from production to emission balance
        em_p2g = self.data_manager.get_value(self.data_manager.static_data, 'prod_em_p2g')
        em_p2g_storage = 0

        creator.emission_balance += self.cap_inv_p2g * em_p2g
        creator.emission_balance += self.cap_inv_p2g_storage * em_p2g_storage

        # store value for export
        creator.inv_dic[self.cap_inv_p2g.name.split('_')[-1]] = [self.inv_cost_p2g, em_p2g]
        creator.inv_dic[self.cap_inv_p2g_storage.name.split('_')[-1]] = [self.inv_cost_storage, em_p2g_storage]

        iteration_hours = self.data_manager.dynamic_data.index.values.tolist()
        for i in iteration_hours:
            self.__add_constraints(i, creator, balance_tracker)

        # same soc at beginning and end of time period (zero)
        self.lp_obj += self.variable_dic[min(iteration_hours)][2] == self.variable_dic[max(iteration_hours)][2]
        # self.lp_obj += self.variable_dic[max(iteration_hours)][2] == 0

        pass

    def __add_constraints(self, time, creator, balance_tracker):
        # build constraints for battery
        # main constraint
        # variables stored in list, ordering 0: gas output, 1: electricity input, 2:current stored energy, 3:binary variable
        self.variable_dic[time] = [pulp.LpVariable('v_g_ptog_' + str(time)), pulp.LpVariable('u_e_ptog_' + str(time)), pulp.LpVariable('stored_g_ptog_' + str(time))]

        # Non-negativity constraints
        self.lp_obj += self.variable_dic[time][0] >= 0
        self.lp_obj += self.variable_dic[time][1] >= 0
        self.lp_obj += self.variable_dic[time][2] >= 0

        # decision: charge or discharge
        # big-M:
        # M = 10000000
        # self.lp_obj += self.variable_dic[time][0] <= M * self.variable_dic[time][3]
        # self.lp_obj += self.variable_dic[time][1] <= M * (1-self.variable_dic[time][3])

        # calculating state of charge
        # stored_gas[t] = stored_gas[t-1] + u_e[t-1] - v_g[t-1]
        if time-1 in self.variable_dic.keys():
            self.lp_obj += self.variable_dic[time][2] == self.variable_dic[time-1][2] - self.variable_dic[time-1][0] + self.eta_p2g *self.variable_dic[time-1][1]
            # subtract CO2 from balance
            creator.emission_balance += - self.eta_p2g * self.variable_dic[time-1][1] * self.data_manager.get_value(
                self.data_manager.static_data, 'g_emissions')

        # Adding to balance
        balance_tracker[time].gas_balance += self.variable_dic[time][0]
        balance_tracker[time].electricity_balance += -self.variable_dic[time][1]


        # defining upper bound constraints
        # conversion rate limited by capacity of p2g
        self.lp_obj += self.variable_dic[time][0] <= self.r_out * (self.cap_ins_p2g + self.cap_inv_p2g)
        self.lp_obj += self.variable_dic[time][1] <= (self.cap_ins_p2g + self.cap_inv_p2g) / self.eta_p2g
        self.lp_obj += 0 <= self.variable_dic[time][2] <= self.cap_ins_gas_storage + self.cap_inv_p2g_storage

        return



