# in this file the actual linear problem will run
# contains loop for timeiteration

import pandas as pd

import pulp
from model_functions import Pumpedhydro
# from files.model_functions import Hydropower
from peroid_handler import Period, Balancer
from data_transfer import DataManager
from model_functions import NuclearPower, Batteries, Hydropower, PowerToGas

from pulp import *
import re


class Manager:
    def __init__(self, data_manager):
        # handles the creation of the LP, iteration over every timeperiod and solving/data output process

        # define data_manager
        self.data_manager = data_manager
        # creaton of the LP
        self.creator = Creator(self.data_manager)
        # balance dictionary to track all balance objects
        self.balance_tracker = {}

        # list of hours
        iteration_hours = self.data_manager.dynamic_data.index.values.tolist()

        # sum for all heat variables of a day
        daily_h_sum = 0
        # iteration through all instances to add time-depending variables, constraints and balances
        print('\n')
        print('Building constraints:')
        for i in iteration_hours:
            # create a Balancer object and store it in balance_tracker
            self.balance_tracker[i] = Balancer(i, self.data_manager)
            # use Period class to sum up all hourly constraints
            Period(self.creator, i, data_manager, self.balance_tracker[i])
            # add hourly heat outputs to daily heat sum
            daily_h_sum += self.balance_tracker[i].heat_balance

            if i % 876 == 0:
                i_perc = round(i / 87.6)
                print(str(i_perc) + '%')

            # daily constraint on heat balance
            # yearly hours from 1 to 8761
            if i % 24 == 0:
                day = i / 24
                self.creator.my_LP += daily_h_sum == self.data_manager.get_value(self.data_manager.heat_demand_data,
                                                                                 day, 'heat_demand')

                daily_h_sum = None

        # call technologies with coherent variables
        # batteries
        if self.creator.switch_batteries:
            # call battery class
            Batteries(self.creator, self.balance_tracker)
        if self.creator.switch_nuclear:
            # call nuclear power class
            NuclearPower(self.creator, self.balance_tracker)
        if self.creator.switch_hydropower:
            # call hydro power class
            Hydropower(self.creator, self.balance_tracker)
        if self.creator.switch_pumped_hydro:
            # call pumped hydro class
            Pumpedhydro(self.creator, self.balance_tracker)
        if self.creator.switch_p2g:
            # call p2g class
            PowerToGas(self.creator, self.balance_tracker)

        # add balances as constraints
        self.creator.add_balance(self.balance_tracker)

        # solve the LP
        # to minimize emissions:
        # call solve_lp('emissions')
        self.creator.solve_lp()

        pass

    def get_data_output(self):
        # get data output from creator object

        # create a directory
        self.data_manager.create_dir()
        self.creator.output_results()
        print('Check data_files/Outputs for  results!')
        print('\n\n\n')
        return


class Creator:
    def __init__(self, data_manager):
        # define input data
        self.data_manager = data_manager
        self.static_data = self.data_manager.static_data

        # emission intensity (assumption: constant value over total timeperiod)
        # import from data files
        self.E_fp = self.data_manager.get_value(self.static_data, 'e_emissions', 'value')
        self.G_fp = self.data_manager.get_value(self.static_data, 'g_emissions', 'value')

        # get technology switches
        self.__enable_technologies()

        # create instances
        self.my_LP = None
        self.total_emissions = None

        # create & solve LP
        self.__create_lp()
        self.__generate_costs_and_emissions()

    pass

    def __create_lp(self):
        # create a pulp LP object and add time-independent variables and constraints (if existing)

        # LP-object
        self.my_LP = LpProblem("SectorCoupling_Optimization", LpMinimize)

        # time independent variables
        self.total_emissions = pulp.LpVariable('total_emissions')
        self.total_costs = pulp.LpVariable('total_costs')

        # time independent constraints
        # emission balance to store all emission variables
        self.emission_balance = None
        # cost balance to store all cost variables
        self.cost_balance = None

        # heatpump
        if self.switch_hp:
            self.cap_inv_hp = pulp.LpVariable('cap_inv_hp')
            self.my_LP += self.cap_inv_hp >= 0

        # boiler
        if self.switch_boiler:
            self.cap_inv_b = pulp.LpVariable('cap_inv_b')
            self.my_LP += self.cap_inv_b >= 0

        # gas turbine
        if self.switch_gasturbine:
            self.cap_inv_gt = pulp.LpVariable("cap_inv_gt")
            self.my_LP += self.cap_inv_gt >= 0

        # PV
        if self.switch_solar:
            self.cap_inv_pv = pulp.LpVariable("cap_inv_pv")
            self.my_LP += self.cap_inv_pv >= 0

        return

    def __enable_technologies(self):
        # enable all technologies
        # print enabled technologies
        print('Involved Technologies: ')
        self.switch_boiler = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_boiler', 'value'))
        print(f'Boiler: {self.switch_boiler}')
        self.switch_hp = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_heatpump', 'value'))
        print(f'Heatpump: {self.switch_hp}')
        self.switch_solar = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_solar', 'value'))
        print(f'Solar Power: {self.switch_solar}')
        self.switch_gasturbine = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_gasturbine', 'value'))
        print(f'Gas turbines: {self.switch_gasturbine}')
        self.switch_batteries = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_batteries', 'value'))
        print(f'Batteries: {self.switch_batteries}')
        self.switch_hydropower = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_hydropower', 'value'))
        print(f'Hydropower: {self.switch_hydropower}')
        self.switch_nuclear = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_nuclear', 'value'))
        print(f'Nuclear power: {self.switch_nuclear}')
        self.switch_pumped_hydro = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_pumpedhydro', 'value'))
        print(f'Pumped hydro power: {self.switch_pumped_hydro}')
        self.switch_ror = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_runofriver', 'value'))
        print(f'Run of River: {self.switch_ror}')
        self.switch_p2g = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_p2g', 'value'))
        print(f'Power-2-Gas: {self.switch_p2g}')
        self.switch_exports = self.data_manager.bool_check(
            self.data_manager.get_string(self.static_data, 'enable_exports', 'value'))
        print(f'Electricity Exports: {self.switch_exports}')
        return

    def __generate_costs_and_emissions(self):
        # generate necessary investment costs and the emissions caused by this investment
        # investment cost = (maximal used capacity - installed capacity) * investment cost / lifetime

        # dictionary to store investment costs
        self.inv_dic = {}

        # boiler investment costs
        if self.switch_boiler:
            # investment cost
            inv_b = float(
                self.data_manager.get_value(self.static_data, 'lic_boiler', 'value'))
            # add investment to cost balance
            self.cost_balance += self.cap_inv_b * inv_b

            # add emissions from production to emission balance
            em_b = float(
                self.data_manager.get_value(self.static_data, 'prod_em_b', 'value'))
            self.emission_balance += self.cap_inv_b * em_b

            # store variable in dictionary
            self.inv_dic[self.cap_inv_b.name.split('_')[-1]] = [inv_b, em_b]

        # heatpump investment costs
        if self.switch_hp:
            inv_hp = float(
                self.data_manager.get_value(self.static_data, 'lic_boiler', 'value'))
            self.cost_balance += self.cap_inv_hp * inv_hp

            em_hp = float(
                self.data_manager.get_value(self.static_data, 'prod_em_hp', 'value'))
            self.emission_balance += self.cap_inv_hp * em_hp

            self.inv_dic[self.cap_inv_hp.name.split('_')[-1]] = [inv_hp, em_hp]

        # PV investment costs (per kW)
        if self.switch_solar:
            inv_solar = float(
                self.data_manager.get_value(self.static_data, 'inv_pv', 'value'))
            self.cost_balance += self.cap_inv_pv * inv_solar
            em_solar = float(
                self.data_manager.get_value(self.static_data, 'prod_em_pv', 'value'))
            self.area_pv = self.data_manager.get_value(self.static_data, 'area_pv', 'value')  # kW/m^2
            # calculate emissions
            # changed inv_solar to self.cap_inv_pv 
            self.emission_balance += self.cap_inv_pv * em_solar
            self.inv_dic[self.cap_inv_pv.name.split('_')[-1]] = [inv_solar, em_solar]

        if self.switch_gasturbine:
            inv_gt = float(
                self.data_manager.get_value(self.static_data, 'lic_gt', 'value'))
            self.cost_balance += self.cap_inv_gt * inv_gt

            em_gt = float(
                self.data_manager.get_value(self.static_data, 'prod_em_gt', 'value'))
            self.emission_balance += self.cap_inv_gt * em_gt

            self.inv_dic[self.cap_inv_gt.name.split('_')[-1]] = [inv_gt, em_gt]

        return

    def add_balance(self, balance_tracker):
        # create a constraint from the balances

        # penalty for exporting electricity
        export_penalty = float(
                self.data_manager.get_value(self.static_data, 'export_penalty', 'value'))

        # iterate over every hour
        for i in self.data_manager.dynamic_data.index.values.tolist():

            if self.switch_exports:
                # export variable
                export = pulp.LpVariable('n_e_' + str(i))
                self.my_LP += export >= 0

                # add to cost
                # small penalty for exports
                self.cost_balance += export * export_penalty

                self.my_LP += balance_tracker[i].electricity_balance == export
            else:
                self.my_LP += balance_tracker[i].electricity_balance == 0
                
            self.my_LP += balance_tracker[i].gas_balance == 0
        return

    def solve_lp(self, func_val='cost'):
        # defines the objective function and solves the LP
        optimize = func_val

        # emission costs
        self.cost_balance += self.total_emissions * self.data_manager.get_value(self.static_data, 'co2_price') / 1000000

        # define cost & emissions constraint
        self.my_LP += self.total_emissions == self.emission_balance
        self.my_LP += self.total_costs == self.cost_balance

        if optimize == 'cost':

            # max emission constraint
            self.my_LP += self.total_emissions <= self.data_manager.get_value(self.data_manager.static_data,
                                                                              'max_total_emissions') * 1000000
            # define objective function
            # min cost
            self.my_LP += self.total_costs
            opt = self.my_LP.objective

        elif optimize == 'emissions':

            # define objective function
            # min emissions
            self.my_LP += self.total_emissions
            opt = self.my_LP.objective

        else:
            raise ValueError(f'Function value {func_val} is not valid. This model is only able minimize costs or '
                             f'emissions.')

        print('Solving...')
        # solve LP
        self.my_LP.solve()
        print('Found solution! Please check if optimal or not!')

        # for testing
        # output
        print(f'Total annual costs: {self.total_costs.value()} CHF')
        print(f'Total annual emissions: {round(self.total_emissions.value() / 1000000, 1)} t CO2\n')

        return

    def output_results(self):
        # get result matrix and optimal solution
        print('Gathering data from variables and writing Output files...')

        # OUTPUT TIME DEPENDING VARIABLES
        # necessary lenght from timeseries
        hours = self.data_manager.dynamic_data.index.values.tolist()

        # write all variable values in a dictionary in chronological order
        variable_dic = {}
        variable_dic_ordered = {}

        tech_involved = {
            'name': [],
            'cap_inst': []
        }

        # get all variables
        for v in self.my_LP.variables():
            if any(char.isdigit() for char in v.name.split('_')[-1]):
                # get time depending variables
                variable_dic[v.name] = v.varValue
            elif 'cap_inv' in v.name:
                # find involved technologies
                tech_involved['name'].append(v.name.split('_')[-1])
                tech_involved['cap_inst'].append(v.varValue)

        # collect all variable groups
        for i in variable_dic:
            variable_name = re.findall('\d*\D+', i)[0][:-1]
            variable_hour = re.findall(r'\d+', i)[0]

            if variable_name not in variable_dic_ordered:
                variable_dic_ordered[variable_name] = [[], []]
            variable_dic_ordered[variable_name][0].append(variable_dic[i])
            variable_dic_ordered[variable_name][1].append(int(variable_hour))

        # sort by hours
        for i in variable_dic_ordered:
            if len(variable_dic_ordered[i][0]) != len(hours):
                raise ValueError(f'Lenght missmatch with variable {i}')
            variable_dic_ordered[i] = [x for _, x in
                                       sorted(zip(variable_dic_ordered[i][1], variable_dic_ordered[i][0]))]

        # add hours
        variable_dic_ordered['hours'] = hours
        # add electricity demand to ouput
        variable_dic_ordered['Electricity demand [kW]'] = self.data_manager.dynamic_data['electricity_demand'].to_list()

        # create dataframe with hours as index
        df = self.data_manager.dic_2_df(variable_dic_ordered, 'hours')
        # write dataframe to csv
        # self.data_manager.write_file(df, 'variable_output_24h')
        self.data_manager.write_df(df, 'variable_output_per_hour')

        # COMPUTE TECHNOLOGY DATA
        tech_variables_values = {'existing capacity [kW]': self.__get_existing_cap(tech_involved['name']),
                                 'additionally installed capacity [kW]': tech_involved['cap_inst'],
                                 'technology': tech_involved['name'],
                                 'investment cost [CHF per year]': self.__get_investment(self.inv_dic,
                                                                                         tech_involved['name'],
                                                                                         tech_involved['cap_inst'], 0),
                                 'emissions by investment [t]': [x / 1000000 for x in
                                                                 self.__get_investment(self.inv_dic,
                                                                                       tech_involved['name'],
                                                                                       tech_involved['cap_inst'], 1)]}

        # add technology abbreviation

        # create dataframe with technology as index
        df2 = self.data_manager.dic_2_df(tech_variables_values, 'technology')

        # write dataframes to csv
        # self.data_manager.write_file(df2, 'technology_output_24h')
        self.data_manager.write_df(df2, 'technology_capacities')

        # get total emissions & costs, variables In- & Outputs
        result_dic = {
            'Total costs [CHF]': self.my_LP.objective.value(),
            'Total emission [t]': round(self.total_emissions.value() / 1000000, 1),
            'Total cost for CO2 permits [CHF]': (self.total_emissions.value() / 1000000) * self.data_manager.get_value(
                self.static_data, 'co2_price')
        }

        # get variable total
        for i in variable_dic_ordered:
            if i != 'hours':
                result_dic[i] = sum(variable_dic_ordered[i])

        # create dataframe
        df3 = pd.DataFrame.from_dict(result_dic, orient='index')

        # write dataframes to csv
        # self.data_manager.write_file(df2, 'technology_output_24h')
        self.data_manager.write_df(df3, 'variable_output_total')

        # copy static data file
        self.data_manager.copy_inputs()

        return

    def __get_existing_cap(self, tech_list):
        # return installed capacities for every technology withhin tech_list
        cap_inst = []
        str = 'cap_inst_'
        for i in tech_list:
            cap_inst.append(self.data_manager.get_value(self.data_manager.static_data, str + i, 'value'))
        return cap_inst

    def __get_investment(self, dic, tech_list, inv_cap, element):
        # return a list of all investment cost per technology
        inv_cost = []
        for (i, n) in zip(tech_list, inv_cap):
            if i in dic.keys():
                inv_cost.append((n * self.inv_dic[i][element]))
            else:
                inv_cost.append(0)
        return inv_cost
