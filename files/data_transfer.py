# add classes to import, edit, shrink, export, etc data

from typing import Final
import pandas
import os
import re
import shutil
from pathlib import Path
import sys

import pandas as pd
from setuptools import setup


class DataManager:
    def __init__(self, setup):
        # contains all functions to get data from data files
        # contains functions to process data (like interpolate, extrapolate, mean, ...; only if needed)
        self.__get_data_inputs(setup)

        pass

    def __get_data_inputs(self, setup):
        # read data input file to dataframe
        # every needed file is read to a dataframe
        # todo: @all

        # read static data
        self.setup = setup
        folder = "../data_files/Inputs/" + setup

        try:
            self.static_data = self.__read_file(os.path.join( os.path.dirname( __file__ ),folder,'static_data.csv'), 'variable_name')
            self.heat_demand_data = self.__read_file(os.path.join( os.path.dirname( __file__ ),folder,'daily_data.csv'), 'day')
            self.dynamic_data = self.__read_file(os.path.join( os.path.dirname( __file__ ),folder,'hourly_data.csv'), 'hour')
            # get time vector of water inflow
            self.hydro_inflow = self.dynamic_data['hydro_inflow'].tolist()
            self.ph_inflow = self.dynamic_data['ph_inflow'].tolist()

        except FileNotFoundError:
            print(f"File couldn't be found {setup}")
            sys.exit()

        return

    def __read_file(self, file_path, index=None):
        # read file from path
        # return dataframe
        df = pd.read_csv(file_path)
        if index:
            # sets column as index if given as input
            if index not in list(df):
                raise ValueError(f"'{index}' is no column in this file!")
            else:
                df = df.set_index(index)
                return df
        pass

    def get_value(self, df, key, column='value'):
        # get a single value from df
        # based on row index and column
        # use only for numbers
        try:
            return float(df.at[key, column])
        except:
            raise ValueError(f"'{key}' or '{column}' does not exist!")

    def get_string(self, df, key, column):
        # get a single value from df
        # based on row index and column
        # use only for numbers
        try:
            return df.at[key, column]
        except:
            raise ValueError(f"'{key}' or '{column}' does not exist!")

    def bool_check(self, value):
        # check if a value is true or false
        # return boolean
        if value.lower() in ['true', 't', '1', 'yes', 'y']:
            return True
        else:
            return False

    def dic_2_df(self, dic, index=False):
        # write a dictionary to a dataframe
        # if index set a column as index
        df = pd.DataFrame.from_dict(dic)

        if index is not False:
            # set column as index
            df = df.set_index(index)
        return df

    def get_vector(self, time):
        # get time depending input vector
        # todo
        pass

    def create_dir(self):
        # create a new folder to store results
        # numbers result directories

        # path to Outputs
        main_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', f'data_files/Outputs/'))

        # Creation of new path
        j = 0
        dir_name = self.setup + '_' + str(j)
        self.output_path = os.path.abspath(os.path.join(main_path, dir_name))

        # keep incrementing j until this file doesn't exist yet
        while os.path.exists(self.output_path):
            j += 1
            dir_name = self.setup + '_' + str(j)
            self.output_path = os.path.abspath(os.path.join(main_path, dir_name)) # dir_name))
            if j==100:
                break

        try:
            os.mkdir(self.output_path)
        except OSError:
            print("Creation of the directory %s failed" % self.output_path)

        return

    def copy_inputs(self):
        # copy static data file to the output folder
        shutil.copy(os.path.join(os.path.dirname(__file__), '../data_files/Inputs',self.setup,'static_data.csv' ), self.output_path)

        return

    def write_df(self, df, file_title):
        # write dataframe to csv-file in Output directory
        # define path

        path = os.path.abspath(os.path.join(self.output_path, f'{file_title}.csv'))
        # write to csv
        try:
            df.to_csv(path)

        except:
            raise print(f'File might be opened or {file_title} is an invalid file name! Input example "test_1" --> test_1.csv')
        pass





