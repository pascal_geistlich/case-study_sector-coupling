# main file: run this file

from lp import Manager
from data_transfer import DataManager
import os

# def loop_dir():
#    # call this function to loop over a directory
#
#    # directory that contains all input folders
#    path = 'nuc_phaseout'
#
#    sub_dir = os.path.join(os.path.dirname(__file__), "../data_files/Inputs/" + path)
#
#    scenarios = [name for name in os.listdir(sub_dir) if os.path.isdir(os.path.join(sub_dir, name))]
#    for i in scenarios:
#        setup = path + '/' + i
#        run_model(setup)
#    return


def run_model(setup):
    # run model with setup data

    print(f'Loading: {setup}')

    data = DataManager(setup)
    model = Manager(data)
    model.get_data_output()

    return


#if __name__ == "__main__":
#    # create Manager object to initialize the LP
#
#    # use this function if you want to run multiple scenarios from a folder
#    loop_dir()

# use directly if you only want to run one scenario
run_model('island') # 'base_case')


