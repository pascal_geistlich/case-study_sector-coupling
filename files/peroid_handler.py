# this file contains all classes that are called in every period / iteration

from data_transfer import DataManager
import model_functions as fun
import pulp


class Balancer:
    def __init__(self, i, data_manager):
        # initialize balances

        # heat balance tracks variables only
        self.heat_balance = None

        self.electricity_balance = 0
        # electricity demand
        self.electricity_balance += - data_manager.get_value(data_manager.dynamic_data, i, 'electricity_demand')
        # no gas demand
        self.gas_balance = 0
        pass




class Period:
        def __init__(self, lp_creator, time, data_manager, balance_obj):
            # collect all balances, variables and constraints of one peroid of the whole loop

            # define inputs
            # redefine creator object
            self.creator = lp_creator
            # lp variable / pulp object
            self.lp_obj = lp_creator.my_LP
            # redefine time
            self.time = time

            # timedepending data input
            # todo
            self.data_manager = data_manager

            # redefine balance object
            self.balance = balance_obj

            self.__define_variables()
            self.__call_technologies()
            pass

        def __define_variables(self):
            # define variables
            # energy imports of peroid i
            self.m_g = pulp.LpVariable('m_g_' + str(self.time))
            self.lp_obj += 0 <= self.m_g

            self.m_e = pulp.LpVariable('m_e_' + str(self.time))
            self.lp_obj += 0 <= self.m_e
            self.lp_obj += self.m_e <= 3000

            # add import to balance
            self.balance.gas_balance += self.m_g
            self.balance.electricity_balance += self.m_e

            # add costs to balance
            self.creator.cost_balance += self.m_g * self.data_manager.get_value(self.creator.static_data, 'g_price',)
            self.creator.cost_balance += self.m_e * self.data_manager.get_value(self.creator.static_data, 'e_price')
            return

        def __call_technologies(self):
            # call involved technologies
            if self.creator.switch_boiler:
                fun.Boiler(self.creator, self.balance, self.time)
            if self.creator.switch_hp:
                fun.Heatpump(self.creator, self.balance, self.time)
            if self.creator.switch_solar:
                fun.Solarpower(self.creator, self.balance, self.time)
            if self.creator.switch_gasturbine:
                fun.GasTurbines(self.creator, self.balance, self.time)
            if self.creator.switch_ror:
                fun.RunOfRiver(self.creator, self.balance, self.time)
            return